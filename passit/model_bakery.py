from model_bakery import baker
from model_bakery.random_gen import gen_slug

baker.generators.add(
    "apps.access.fields.PublicKeyField", "passit.test_utils.dummy_gen"
)
baker.generators.add(
    "apps.access.fields.CiphertextField", "passit.test_utils.dummy_gen"
)
baker.generators.add("organizations.fields.SlugField", gen_slug)