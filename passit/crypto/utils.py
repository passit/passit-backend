from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from django.utils.encoding import force_bytes
import base64


def make_key_from_password(salt, password):
    """Generate a key from a salt and password

    The password is human typable. We want to make it stronger using a
    key derivation function. This is only used to decrypt a RSA private key
    and so this function is for unit testing only.
    https://cryptography.io/en/latest/fernet/#using-passwords-with-fernet
    """
    salt = force_bytes(salt)
    password = force_bytes(password)

    kdf = PBKDF2HMAC(
        algorithm=hashes.SHA256(),
        length=32,
        salt=salt,
        iterations=100000,
        backend=default_backend(),
    )
    key = base64.urlsafe_b64encode(kdf.derive(password))
    return key.decode()
