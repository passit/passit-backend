from django.conf.urls import include
from django.urls import path, re_path
from django.contrib import admin
from django.conf import settings
from django.views.generic import TemplateView
from rest_framework import routers
from apps.access.views import (
    UserPublicAuthView,
    UserViewSet,
    GroupViewSet,
    GroupUserViewSet,
    UsernameAvailableView,
    UserLookUpView,
    UserPublicKeyView,
    MyContactsViewSet,
    PingView,
    LookupUserIdView,
    AcceptGroupInviteView,
    RejectGroupInviteView,
    UserResetPasswordView,
    UserResetPasswordVerifyView,
)
from apps.secrets.views import (
    SecretViewSet,
    SecretThroughViewSet,
    ChangePasswordView,
    ChangePasswordBackupView,
)
from apps.conf.views import ConfView
from apps.confirm_email.views import (
    confirm_email_from_long_code,
    confirm_email_from_short_code,
    request_new_confirmation,
)
from apps.auth.views import BackupCodeLoginView
from apps.mfa.views import (
    GenerateMFAView,
    VerifyMFAView,
    VerifyFIDO2View,
    ActivateMFAView,
    DeactivateMFAView,
    CustomUserKeyViewSet,
)
from apps.organizations_ext.views import OrganizationViewSet
from .views import health
from django_rest_mfa.views import UserKeyViewSet


router = routers.DefaultRouter()
router.register(r"users", UserViewSet, basename="users")
router.register(r"contacts", MyContactsViewSet, basename="contacts")
router.register(r"groups", GroupViewSet, basename="groups")
router.register(r"groups/(?P<id>\d+)/users", GroupUserViewSet, basename="group-users")
router.register(r"secrets", SecretViewSet, basename="secrets")
router.register(
    r"secrets/(?P<id>\d+)/groups", SecretThroughViewSet, basename="secret-groups"
)
router.register(r"organizations", OrganizationViewSet, basename="organizations")
router.register(r"user_keys", CustomUserKeyViewSet, basename="user-keys")

urlpatterns = [
    re_path(r"_health/", health),
    re_path(r"^api/", include(router.urls)),
    re_path(r"^api/ping/$", PingView.as_view(), name="ping"),
    re_path(
        r"^api/lookup-user-id/$", LookupUserIdView.as_view(), name="lookup-user-id"
    ),
    re_path(
        r"^api/user-public-auth/(?P<email>[\w.%+-]+@[A-Za-z0-9.-]+\.[A-Za-z0-9]{2,63})",
        UserPublicAuthView.as_view(),
        name="user-public-auth",
    ),
    re_path(
        r"^api/username-available/(?P<email>[\w.%+-]+@[A-Za-z0-9.-]+\.[A-Za-z0-9]{2,63})",
        UsernameAvailableView.as_view(),
        name="username-available",
    ),
    re_path(
        r"^api/user-lookup/(?P<email>[\w.%+-]+@[A-Za-z0-9.-]+\.[A-Za-z0-9]{2,63})",
        UserLookUpView.as_view(),
        name="user-available",
    ),
    re_path(
        r"^api/user-public-key/(?P<id>\d+)/$",
        UserPublicKeyView.as_view(),
        name="user-public-key",
    ),
    re_path(
        r"^api/change-password/", ChangePasswordView.as_view(), name="change-password"
    ),
    re_path(
        r"^api/change-password-backup/",
        ChangePasswordBackupView.as_view(),
        name="change-password-backup",
    ),
    re_path(
        r"^api/accept-group-invite/$",
        AcceptGroupInviteView.as_view(),
        name="accept-group-invite",
    ),
    re_path(
        r"^api/reject-group-invite/$",
        RejectGroupInviteView.as_view(),
        name="reject-group-invite",
    ),
    re_path(
        r"^api/confirm-email-long-code/",
        confirm_email_from_long_code,
        name="confirm-email-long-code",
    ),
    re_path(
        r"^api/confirm-email-short-code/",
        confirm_email_from_short_code,
        name="confirm-email-short-code",
    ),
    re_path(
        r"^api/request-new-confirmation/",
        request_new_confirmation,
        name="request-new-confirmation",
    ),
    re_path(r"^api/conf/", ConfView.as_view(), name="conf"),
    re_path(r"^api/generate-mfa/", GenerateMFAView.as_view(), name="generate-mfa"),
    re_path(r"^api/verify-mfa/", VerifyMFAView.as_view(), name="verify-mfa"),
    re_path(r"^api/verify-fido2/", VerifyFIDO2View.as_view(), name="verify-fido2"),
    re_path(r"^api/activate-mfa/", ActivateMFAView.as_view(), name="activate-mfa"),
    re_path(
        r"^api/deactivate-mfa/", DeactivateMFAView.as_view(), name="deactivate-mfa"
    ),
    re_path(
        r"^api/auth/", include("apps.auth.urls")
    ),  # https://james1345.github.io/django-rest-knox/auth/#global-usage-on-all-views
    re_path(
        r"^api/reset-password/", UserResetPasswordView.as_view(), name="reset-password"
    ),
    re_path(
        r"^api/reset-password-verify/",
        UserResetPasswordVerifyView.as_view(),
        name="reset-password-verify",
    ),
    re_path(
        r"^api/reset-password-login/",
        BackupCodeLoginView.as_view(),
        name="reset-password-login",
    ),
    re_path(r"^$", TemplateView.as_view(template_name="index.html")),
    # These routes belong to the Angular single page app
    re_path(
        r"^(login|account|register|confirm-email|list|groups|import|export).*$",
        TemplateView.as_view(template_name="index.html"),
    ),
]

if settings.ENABLE_DJANGO_ADMIN:
    urlpatterns = [
        re_path(r"^admin/", admin.site.urls),
    ] + urlpatterns

if settings.BILLING_ENABLED:
    urlpatterns += [
        path("stripe/", include("djstripe.urls", namespace="djstripe")),
    ]
