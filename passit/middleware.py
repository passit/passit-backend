from server_side_matomo.middleware import MatomoMiddleware


class PassitMatomoMiddleware(MatomoMiddleware):
    def __call__(self, request):
        """Implements a url blacklist before running Matomo middleware"""
        path = request.get_full_path()
        blacklist = (
            "/api/user-public-auth/",
            "/api/ping/",
        )
        if path.startswith(blacklist):
            return self.get_response(request)
        return super().__call__(request)
