from django.conf import settings
from rest_framework import permissions
from apps.mfa.permissions import MFAVerify


class HasVerifiedEmail(permissions.BasePermission):
    """Ensure user is email verified"""

    message = "User's email is not confirmed."

    def has_permission(self, request, view):
        if settings.IS_TEST_MODE is True:
            return True
        return request.user and getattr(request.user, "email_confirmed", False)


DEFAULT_PERMISSIONS = [permissions.IsAuthenticated, HasVerifiedEmail, MFAVerify]
