from django.core.cache import cache
from django.conf import settings
from rest_framework.authentication import BasicAuthentication
from rest_framework.exceptions import AuthenticationFailed
from apps.ldap.authentication import check_ldap


# Why 50? You can't reset your password with passit. It's reasonable for a human to try
# logging in many times. We also hash passwords both client side (and again in backend)
# This results in a super long "password" sent to the server. A brute force attack must
# either attack the long password (very hard) or guess the user's short password but hash it
# every time (computationally very expensive)
MAX_ATTEMPTS = 50
TIMEOUT = 600


class QuietBasicAuthentication(BasicAuthentication):
    def authenticate_header(self, request):
        """https://richardtier.com/2014/03/06/110/"""
        return 'xBasic realm="%s"' % self.www_authenticate_realm

    def authenticate_credentials(self, userid, password, request=None):
        """Stop brute force attempts"""
        auth_failure_key = "LOGIN_FAILURES_FOR_%s" % userid
        auth_failures = cache.get(auth_failure_key) or 0
        if auth_failures >= MAX_ATTEMPTS:
            raise AuthenticationFailed("Locked out: too many authentication failures")

        try:
            result = super().authenticate_credentials(userid, password, request)
        except AuthenticationFailed as e:
            cache.set(auth_failure_key, auth_failures + 1, TIMEOUT)
            raise e

        if settings.LDAP_ENABLED:
            isValid, errorMessage = check_ldap(userid)
            if isValid is False:
                cache.set(auth_failure_key, auth_failures + 1, TIMEOUT)
                raise AuthenticationFailed(errorMessage)
        return result
