from django.db import models


class LDAPSync(models.Model):
    """Represents one LDAP server for syncing user data with"""

    organization = models.ForeignKey(
        "organizations_ext.Organization", on_delete=models.CASCADE
    )
    email_domain = models.CharField(
        max_length=254,
        help_text="Check users with this email domain against this LDAP server. Example: `example.com` would make the user person@example.com check against this LDAP server.",
    )
    ldap_uri = models.CharField(
        max_length=1000, help_text="Example: ldap://ldap.example.com"
    )
    bind_user = models.CharField(
        max_length=1000,
        help_text="LDAP Binding Account to authenticate to server to. Preferably this account should have read only access. Example: cn=my-user,dc=example,dc=com",
    )
    bind_password = models.CharField(
        max_length=1000,
        blank=True,
        help_text="Password for bind_user. Note this is saved unencrypted.",
    )
    base_dn = models.CharField(
        max_length=1000,
        help_text="Base DN for LDAP search when looking up users. Example: ou=people,dc=example,dc=com",
    )
    filters = models.CharField(
        max_length=1000,
        blank=True,
        help_text="Filter for LDAP search. Example: (objectclass=person)",
    )
    email_attribute = models.CharField(
        max_length=100,
        blank=True,
        help_text="LDAP attribute to match to Passit email address. Example: mail. If left blank, Passit will guess the value.",
    )
    first_name_attribute = models.CharField(
        max_length=100,
        blank=True,
        help_text="LDAP attribute to match to Passit first name (given name). Example: gn. If left blank, Passit will guess the value",
    )
    last_name_attribute = models.CharField(
        max_length=100,
        blank=True,
        help_text="LDAP attribute to match to Passit last name (surname). Example: sn. If left blank, Passit will guess the value",
    )

    class Meta:
        verbose_name = "LDAP Sync Setting"
