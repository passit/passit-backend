import ldap
from typing import Tuple

from .models import LDAPSync


def check_ldap(username) -> Tuple[bool, str]:
    """
    Find if there is an LDAP setting that matches the username/email domain.
    If so, check that LDAP server if the user is valid
    If not, ignore it. Non LDAP users are always allowed in (for now, this is not configurable)
    Returns isValid, errorMessage
    """
    domain = username.split("@")[-1]

    ldap_settings = LDAPSync.objects.filter(email_domain=domain).first()
    if not ldap_settings:
        return True, ""

    try:
        conn = ldap.initialize(ldap_settings.ldap_uri)
        conn.start_tls_s()
    except ldap.LDAPError:
        return False, "Unable to contact LDAP server"
    try:
        conn.simple_bind_s(ldap_settings.bind_user, ldap_settings.bind_password)
    except ldap.LDAPError:
        return False, "Unable to authenticate to LDAP server"
    ldap_filter = "(&{}(mail={}))".format(ldap_settings.filters, username)
    try:
        res = conn.search_s(ldap_settings.base_dn, ldap.SCOPE_SUBTREE, ldap_filter, [])
    except ldap.LDAPError:
        return False, "Unable to search LDAP server"
    if len(res) < 1:
        return False, "User not found in LDAP server"
    return True, ""
