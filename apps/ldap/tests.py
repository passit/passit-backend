from django.test import TestCase, override_settings
from model_bakery import baker
from unittest.mock import patch
from ldap.ldapobject import LDAPObject
from .authentication import check_ldap
from .models import LDAPSync


class MockLDAPObject(LDAPObject):
    def __init__(self):
        return

    def start_tls_s(self):
        return

    def simple_bind_s(self, *args):
        return

    def search_s(self, *args):
        return [{}]


class LDAPTestCase(TestCase):
    @override_settings(LDAP_ENABLED=True)
    def test_check_ldap(self):
        """Test a successful ldap check, by mocking ldap"""
        email_domain = "example.com"
        email = "hello@" + email_domain
        baker.make(LDAPSync, email_domain=email_domain)

        with patch("ldap.initialize", return_value=MockLDAPObject()):
            isValid, message = check_ldap(email)
        self.assertTrue(isValid)

    @override_settings(LDAP_ENABLED=True)
    def test_check_ldap_fail(self):
        """Test that we can't connect to an invalid ldap server"""
        email_domain = "example.com"
        email = "hello@" + email_domain
        baker.make(LDAPSync, email_domain=email_domain)
        isValid, message = check_ldap(email)
        self.assertFalse(isValid)
