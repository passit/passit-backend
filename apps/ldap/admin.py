from django.contrib import admin
from .models import LDAPSync


class LDAPSyncInline(admin.StackedInline):
    model = LDAPSync
    extra = 0
