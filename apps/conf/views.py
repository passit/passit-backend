from django.conf import settings
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions


class ConfView(APIView):
    """A way for the client to get global server configuration."""

    permission_classes = [permissions.AllowAny]

    def get(self, request, format=None):
        data = {
            "IS_PRIVATE_ORG_MODE": settings.IS_PRIVATE_ORG_MODE,
            "ENVIRONMENT": settings.ENVIRONMENT,
            "RAVEN_DSN": settings.SENTRY_DSN,
            "SENTRY_DSN": settings.SENTRY_DSN,
            "BILLING_ENABLED": settings.BILLING_ENABLED,
        }
        return Response(data)
