from django.urls import reverse
from django.test import override_settings
from rest_framework.test import APITestCase


class ConfTestCase(APITestCase):
    @override_settings(IS_PRIVATE_ORG_MODE=False)
    def test_conf(self):
        url = reverse("conf")
        response = self.client.get(url)
        self.assertFalse(response.data["IS_PRIVATE_ORG_MODE"])

    @override_settings(SENTRY_DSN="fake_dsn")
    def test_conf_raven_dsn(self):
        url = reverse("conf")
        response = self.client.get(url)
        self.assertEqual(response.data["SENTRY_DSN"], "fake_dsn")

    @override_settings(ENVIRONMENT="staging")
    def test_conf_environment(self):
        """Environment let's the client know if it's connected to production
        or staging environment. Technically it can be any string. Used in error
        reporting."""
        url = reverse("conf")
        response = self.client.get(url)
        self.assertEqual(response.data["ENVIRONMENT"], "staging")
