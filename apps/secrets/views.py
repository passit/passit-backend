from django.core.exceptions import PermissionDenied
from django.contrib.auth.signals import user_logged_in
from django.shortcuts import get_object_or_404
from knox.models import AuthToken
from knox.settings import knox_settings
from rest_framework import viewsets, mixins, status, permissions
from rest_framework.views import APIView
from rest_framework.response import Response

from .serializers import (
    SecretSerializer,
    SecretThroughSerializer,
    ChangePasswordSerializer,
    ChangePasswordBackupSerializer,
)
from .models import Secret, SecretThrough
from apps.auth.permissions import HasVerifiedEmail
from apps.access.models import BackupCodeEmailToken


class SecretViewSet(viewsets.ModelViewSet):
    """Manage personal and group secrets.

    Secrets must be stored in a key value json structure like

        `{"password": "hunter2", "security answer": "The Moon"}`
    Note that nested json is not supported.

    ## Create or update a personal secret

    1. Ensure you have your own public key.
    2. Generate a new AES key. Do not reuse AES keys.
    3. Encrypt the new AES key with your public key.
    4. Encrypt each value in the key value structure. Example:

        `{"password": "cyphertext1", "security answer": "cyphertext2"}`

    5. Submit everything. Use POST for a new secret. Use PUT to update existing
    Example:

            {
                "name": "My site",
                "type": "website",
                "data": {"url": "www.example.com"},
                "secret_through_set": [
                    {
                        "hidden_data": {
                            "password": "cyphertext1",
                            "security answer": "cyphertext2"
                        },
                        "encrypted_key": "aes_key_ciphertext"
                    }
                ]
            }

    ## Create or update group secrets
    1. Get group(s) public key - using groups api.
    2. Generate a new AES key. Do not reuse AES keys.
    3. Encrypt the new AES key with the group's public keys.
    Do this for each group.
    4. Encrypt each value in the key value structure. See personal secret for
    example. Also do this for each group. If the secret is shared with five
    groups then you should have five different AES keys and five sets of secret
    ciphertext.
    5. Submit everything. Example:

            {
                "name": "My site",
                "type": "website",
                "data": {"url": "www.example.com"},
                "secret_through_set": [
                    {
                        "hidden_data": {
                            "password": "cyphertext1",
                            "security answer": "cyphertext2"
                        },
                        "encrypted_key": "aes_key_ciphertext",
                        "group": "3"
                    },
                    {
                        "hidden_data": {
                            "password": "cyphertext3",
                            "security answer": "cyphertext4"
                        },
                        "encrypted_key": "different_aes_key_ciphertext",
                        "group": "4"
                    }
                ]
            }
    """

    queryset = Secret.objects.all()
    serializer_class = SecretSerializer

    def get_queryset(self):
        return Secret.objects.active(self.request.user).distinct()


class SecretThroughViewSet(
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    """SecretThrough can also be thought of like GroupSecrets or UserSecrets.
    Create, list, and destroy. Can't be used to update. Updating would require
    remaking ALL secret throughs so that all users get the update.
    """

    serializer_class = SecretThroughSerializer

    def get_queryset(self):
        my_secrets = Secret.objects.mine(self.request.user)
        secret_id = self.kwargs.get("id")
        queryset = SecretThrough.objects.filter(secret__in=my_secrets)
        queryset = queryset.filter(secret_id=secret_id)
        return queryset

    def perform_create(self, serializer):
        my_secrets = Secret.objects.mine(self.request.user)
        secret_id = self.kwargs.get("id")

        if my_secrets.filter(id=secret_id).exists():
            secret = get_object_or_404(Secret, pk=self.kwargs.get("id"))
            serializer.save(secret=secret)
        else:
            raise PermissionDenied


class ChangePasswordView(APIView):
    serializer_class = ChangePasswordSerializer

    def confirm_identity(self, request, serializer):
        return request.user.check_password(
            serializer.validated_data.get("old_password")
        )

    def post(self, request, format=None):
        # Use context for data because validation strips id fields :(
        # DRF can't deal well with nested writable data
        try:
            serializer = self.serializer_class(
                data=request.data,
                context={
                    "user": request.user,
                    "secret_through_set": request.data["secret_through_set"],
                    "group_user_set": request.data["group_user_set"],
                },
            )
        except KeyError:
            return Response({}, status=status.HTTP_400_BAD_REQUEST)
        if serializer.is_valid():
            if not self.confirm_identity(request, serializer):
                return Response(
                    {"old_password": ["Wrong password."]},
                    status=status.HTTP_400_BAD_REQUEST,
                )
            serializer.save()
            # Expire all tokens
            request.user.auth_token_set.all().delete()
            # Create new token
            _, token = AuthToken.objects.create(request.user)
            user_logged_in.send(
                sender=request.user.__class__, request=request, user=request.user
            )
            UserSerializer = knox_settings.USER_SERIALIZER
            context = {
                "request": self.request,
                "format": self.format_kwarg,
                "view": self,
            }
            return Response(
                {
                    "success": True,
                    "token": token,
                    "user": UserSerializer(request.user, context=context).data,
                },
                status=status.HTTP_200_OK,
            )
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ChangePasswordBackupView(ChangePasswordView):
    """Only used when changing password due to "forgot your password" with backup
    code. Does not require old password (for obvious reasons) but does require
    email backup token.
    """

    serializer_class = ChangePasswordBackupSerializer
    permission_classes = (
        permissions.IsAuthenticated,
        HasVerifiedEmail,
    )

    def confirm_identity(self, request, serializer):
        code = serializer.validated_data.get("code")
        result = BackupCodeEmailToken.check_code(request.user.email, code)
        if result:
            # A successful backup code will always deactivate MFA
            request.user.userkey_set.all().delete()
        return result
