from rest_framework.exceptions import APIException


class ChangePasswordConflictException(APIException):
    """This can happen when client and server hashes mismatch (indicating client has stale data)"""

    status_code = 409
    default_detail = "Server data has changed since client began change password process. Please try again with fresh data."
