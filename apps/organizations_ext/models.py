from organizations.models import Organization as BaseOrganization


class Organization(BaseOrganization):
    class Meta:
        proxy = True

    @property
    def email(self):
        if self.owner:
            return self.owner.organization_user.user.email
