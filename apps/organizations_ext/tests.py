from django.urls import reverse
from rest_framework.test import APITestCase
from model_bakery import baker
from .models import Organization


class OrganizationTestCase(APITestCase):
    def setUp(self):
        self.user = baker.make("access.User")
        self.client.force_login(self.user)
        self.list_url = reverse("organizations-list")

    def test_create(self):
        data = {"name": "Test Org"}
        res = self.client.post(self.list_url, data)
        self.assertContains(res, data["name"], status_code=201)
        organization = Organization.objects.first()
        self.assertEqual(organization.email, self.user.email)

    def test_list(self):
        organization = baker.make(Organization)
        organization.add_user(self.user)
        other_organization = baker.make(Organization)

        res = self.client.get(self.list_url)
        self.assertContains(res, organization.name)
        self.assertNotContains(res, other_organization.name)

    def test_update(self):
        organization = baker.make(Organization)
        organization.add_user(self.user)
        other_organization = baker.make(Organization)
        other_user = baker.make("access.User")
        other_organization.add_user(other_user)
        other_organization.add_user(self.user)

        data = {"name": "New Name"}
        res = self.client.put(
            reverse("organizations-detail", kwargs={"pk": organization.pk}), data
        )
        self.assertContains(res, data["name"])

        res = self.client.put(
            reverse("organizations-detail", kwargs={"pk": other_organization.pk}), data
        )
        self.assertNotContains(res, other_organization.name, status_code=403)
