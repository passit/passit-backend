from django.contrib import admin
from organizations.admin import OrganizationAdmin
from organizations.models import Organization as BaseOrganization
from .models import Organization
from apps.ldap.admin import LDAPSyncInline

admin.site.unregister(BaseOrganization)


@admin.register(Organization)
class OrganizationAdmin(OrganizationAdmin):
    inlines = OrganizationAdmin.inlines + [LDAPSyncInline]
