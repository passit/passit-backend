from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.exceptions import PermissionDenied
from .models import Organization
from .serializers import OrganizationSerializer


class OrganizationViewSet(viewsets.ModelViewSet):
    queryset = Organization.objects.all()
    serializer_class = OrganizationSerializer

    def get_queryset(self):
        return self.queryset.filter(users=self.request.user)

    def perform_create(self, serializer):
        organization = serializer.save()
        organization.add_user(self.request.user)

    def check_permissions(self, request):
        if self.action in ["update", "partial_update", "destory"]:
            if not request.user.organizations_organizationuser.filter(
                organization__pk=self.kwargs.get("pk"), is_admin=True
            ).exists():
                raise PermissionDenied("Only organization admins may edit.")
        return super().check_permissions(request)
