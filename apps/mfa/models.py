from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _
from knox.models import AuthToken


class OTPVerifiedToken(models.Model):
    otp = models.ForeignKey(
        "django_rest_mfa.UserKey",
        related_name="otp_verifications",
        on_delete=models.CASCADE,
    )
    auth_token = models.ForeignKey(
        AuthToken, related_name="otp_verifications", on_delete=models.CASCADE
    )

    class Meta:
        unique_together = ("otp", "auth_token")
