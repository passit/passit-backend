from rest_framework import permissions


class MFAVerify(permissions.BasePermission):
    """Only allow when user either has no MFA set up, or is authorized via MFA"""

    message = "This user requires MFA verification."

    def has_permission(self, request, view):
        user = request.user
        if user.is_anonymous:
            return False
        if not user.mfa_required:
            return True
        token = request.auth
        if token:
            return token.otp_verifications.exists()
        return False
