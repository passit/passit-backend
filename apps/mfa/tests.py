from django.urls import reverse
from django.core import mail
from rest_framework import status
from rest_framework.test import APITestCase, APIRequestFactory
from rest_framework.views import APIView
from rest_framework.response import Response

from model_bakery import baker
from knox.models import AuthToken
from django_rest_mfa.models import UserKey

from apps.access.models import User, BackupCodeEmailToken
from .models import OTPVerifiedToken
from .permissions import MFAVerify

import pyotp
import base64
import json
from datetime import timedelta
from simple_asym import Asym
from simple_asym.crypto import export_private_key, authenticated_encryption


class SimpleView(APIView):
    permission_classes = [MFAVerify]

    def get(self, request, format=None):
        return Response()


factory = APIRequestFactory()
simple_view = SimpleView.as_view()


class MFATestCase(APITestCase):
    def generate_token(self, user):
        ttl = timedelta(hours=1)
        return AuthToken.objects.create(user, ttl)

    def test_generate_mfa(self):
        """Can generate a new mfa secret for logged in user"""
        user = baker.make(User)
        self.client.force_login(user)
        url = reverse("generate-mfa")

        res = self.client.post(url)
        user_otp = UserKey.objects.filter(user=user)

        self.assertTrue(user_otp.exists())
        self.assertContains(res, user_otp.first().properties.get("secret_key"))

    def test_activate_token(self):
        user = baker.make(User)
        otp = baker.make(
            UserKey,
            user=user,
            key_type=UserKey.KeyType.TOTP,
            properties={"is_active": False, "secret_key": "AAAAAAAA"},
        )
        _, token = self.generate_token(user)
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token)
        url = reverse("activate-mfa")
        totp = pyotp.TOTP(otp.properties["secret_key"])
        data = {"otp": totp.now(), "id": otp.id}
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, 200)

        # Should be able to make other API calls now, current token should be approved.
        request = factory.get("/", HTTP_AUTHORIZATION="Token " + token)
        response = simple_view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_verify_mfa(self):
        user = baker.make(User)
        otp = baker.make(
            UserKey,
            user=user,
            key_type=UserKey.KeyType.TOTP,
            properties={"is_active": True, "secret_key": "AAAAAAAA"},
        )
        # Simulate some other otp objects
        baker.make(UserKey, _quantity=3)
        _, token = self.generate_token(user)
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token)
        url = reverse("verify-mfa")

        # Should request actual totp
        res = self.client.post(url, {"otp": "abc"})
        self.assertEqual(res.status_code, 400)

        # Client would generate totp in real circumstances
        totp = pyotp.TOTP(otp.properties.get("secret_key"))
        data = {"otp": totp.now()}
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, 200)

        # No replay attacks
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, 400)

    def test_multiple_token_verify(self):
        """One user may have multiple verified auth tokens"""
        user = baker.make(User)
        otp = baker.make(
            UserKey,
            user=user,
            key_type=UserKey.KeyType.TOTP,
            properties={"is_active": True, "secret_key": "AAAAAAAA"},
        )
        token1, _ = self.generate_token(user)
        _, token2_string = self.generate_token(user)
        baker.make(OTPVerifiedToken, otp=otp, auth_token=token1)
        url = reverse("verify-mfa")

        self.client.credentials(HTTP_AUTHORIZATION="Token " + token2_string)

        totp = pyotp.TOTP(otp.properties.get("secret_key"))
        data = {"otp": totp.now()}
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, 200)

    def test_mfa_permission(self):
        user = baker.make(User)
        tokenObj, token = self.generate_token(user)

        # No MFA should allow
        request = factory.get("/", HTTP_AUTHORIZATION="Token " + token)
        response = simple_view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # With MFA should not allow
        otp = baker.make(
            UserKey,
            user=user,
            key_type=UserKey.KeyType.TOTP,
            properties={"is_active": True, "secret_key": "AAAAAAAA"},
        )
        request = factory.get("/", HTTP_AUTHORIZATION="Token " + token)
        response = simple_view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # With MFA and Verify - should allow
        baker.make(OTPVerifiedToken, otp=otp, auth_token=tokenObj)
        request = factory.get("/", HTTP_AUTHORIZATION="Token " + token)
        response = simple_view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_mfa_permission_perf(self):
        user = baker.make(User)
        tokenObj, token = self.generate_token(user)
        with self.assertNumQueries(4):
            request = factory.get("/", HTTP_AUTHORIZATION="Token " + token)
            simple_view(request)

        otp = baker.make(
            UserKey,
            user=user,
            key_type=UserKey.KeyType.TOTP,
            properties={"is_active": True, "secret_key": "AAAAAAAA"},
        )
        baker.make(OTPVerifiedToken, otp=otp, auth_token=tokenObj)
        # Could I get this down to 4 perhaps...
        with self.assertNumQueries(5):
            request = factory.get("/", HTTP_AUTHORIZATION="Token " + token)
            simple_view(request)

    def test_mfa_deactivate(self):
        user = baker.make(User)
        otp = baker.make(
            UserKey,
            user=user,
            key_type=UserKey.KeyType.TOTP,
            properties={"is_active": True, "secret_key": "AAAAAAAA"},
        )

        token, token_string = self.generate_token(user)
        baker.make(OTPVerifiedToken, otp=otp, auth_token=token)
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token_string)

        url = reverse("deactivate-mfa")
        res = self.client.post(url)
        self.assertEqual(res.status_code, 200)
        self.assertEqual(OTPVerifiedToken.objects.count(), 0)
        request = factory.get("/", HTTP_AUTHORIZATION="Token " + token_string)
        response = simple_view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_mfa_deactive_via_backup_code(self):
        """
        The backup code process should also deactivate mfa status. This is how users should
        gain entry to their account if their mfa code is lost.
        """
        asym = Asym()
        user_password = "hunter2"
        user_backup_code = "d03600f3d385b7"
        private_key, public_key = asym.make_rsa_keys(password=user_password)
        private_key_backup = export_private_key(asym.key_pair, user_backup_code)
        user = baker.make(
            User,
            private_key=private_key,
            public_key=public_key,
            private_key_backup=private_key_backup,
        )
        otp = baker.make(
            UserKey,
            user=user,
            key_type=UserKey.KeyType.TOTP,
            properties={"is_active": True, "secret_key": "AAAAAAAA"},
        )
        self.assertEqual(UserKey.objects.count(), 1)

        # Request backup
        url = reverse("reset-password")
        data = {"email": user.email}
        self.client.post(url, data)

        # Ensure code sent to email
        code = BackupCodeEmailToken.objects.filter(user=user).first()
        self.assertIn(code.code, mail.outbox[-1].body)

        # Verify reset password code
        url = reverse("reset-password-verify")
        data = {
            "email": user.email,
            "code": code.code,
        }
        res = self.client.post(url, data)

        # Decrypt private key with backup code
        asym.set_key_pair(
            res.data["public_key"], res.data["private_key_backup"], user_backup_code
        )
        # Encrypt something to prove access to private key
        random_string = "a" * 20
        encrypted_message = authenticated_encryption(
            base64.b64decode(res.data["server_public_key"]),
            asym.key_pair["private_key"],
            random_string.encode(),
        )

        # Send message to server to prove private key access
        url = reverse("reset-password-login")
        data = {
            "email": user.email,
            "code": code.code,
            "message": encrypted_message,
        }
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, 200)
        self.client.force_login(user)

        # Change password using backup method
        url = reverse("change-password-backup")
        data = {
            "secret_through_set": [],
            "group_user_set": [],
            "code": code.code,
            "user": {
                "client_salt": "a" * 24,
                "password": "a",
                "private_key": "a" * 120,
                "private_key_backup": "a" * 120,
                "public_key": "a" * 44,
            },
        }
        res = self.client.post(url, json.dumps(data), content_type="application/json")
        self.assertEqual(res.status_code, 200)

        self.assertEqual(OTPVerifiedToken.objects.count(), 0)
        self.assertEqual(UserKey.objects.count(), 0)
        _, token_string = self.generate_token(user)
        request = factory.get("/", HTTP_AUTHORIZATION="Token " + token_string)
        response = simple_view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
