from apps.auth.permissions import HasVerifiedEmail
from apps.mfa.models import OTPVerifiedToken
from django.shortcuts import get_object_or_404
from django.utils import timezone
from django_rest_mfa.fido2 import get_server, get_user_credentials
from django_rest_mfa.helpers import set_expirable_var
from django_rest_mfa.models import UserKey
from django_rest_mfa.totp import generate_totp_key, verify_totp
from django_rest_mfa.views import FIDO2Authenticate, UserKeyViewSet
from knox.auth import TokenAuthentication
from rest_framework import permissions, status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.throttling import UserRateThrottle
from rest_framework.views import APIView

from .serializers import ActivateMFASerializer, VerifyMFASerializer


class GenerateMFAView(APIView):
    def post(self, request, format=None):
        key = generate_totp_key(request.user)
        user_key = UserKey.objects.create(
            user=request.user,
            key_type=UserKey.KeyType.TOTP,
            name="TOTP",
            properties={"secret_key": key["secret_key"], "is_active": False},
        )
        result = {"provisioning_uri": key["provisioning_uri"], "id": user_key.pk}
        return Response(result)


class ActivateMFAView(APIView):
    """Activate an OTP object, making it begin validating authentication"""

    def post(self, request, format=None):
        serializer = ActivateMFASerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        otp = get_object_or_404(
            UserKey,
            pk=serializer.data["id"],
            user=request.user,
            key_type=UserKey.KeyType.TOTP,
        )
        is_valid = verify_totp(otp.properties["secret_key"], serializer.data["otp"], 30)
        if is_valid:
            otp.properties["is_active"] = True
            otp.save()
            request.auth.otp_verifications.create(otp=otp)
            return Response()
        return Response(status=status.HTTP_400_BAD_REQUEST)


class DeactivateMFAView(APIView):
    """Deactivate an OTP object, making the user no longer require MFA"""

    def post(self, request, format=None):
        request.user.userkey_set.filter(key_type=UserKey.KeyType.TOTP).delete()
        return Response()


class Burst10MinUser(UserRateThrottle):
    rate = "10/minute"


class VerifyMFAView(APIView):
    throttle_classes = (Burst10MinUser,)
    authentication_classes = (TokenAuthentication,)
    permission_classes = (
        permissions.IsAuthenticated,
        HasVerifiedEmail,
    )

    def post(self, request, format=None):
        serializer = VerifyMFASerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user_otp = request.user.userkey_set.filter(
            properties__is_active=True, key_type=UserKey.KeyType.TOTP
        ).first()
        if user_otp and user_otp.properties.get("last_otp") != serializer.data["otp"]:
            is_valid = verify_totp(
                user_otp.properties["secret_key"], serializer.data["otp"], 1
            )
            if is_valid:
                OTPVerifiedToken.objects.get_or_create(
                    otp=user_otp, auth_token=request.auth
                )
                user_otp.properties["last_otp"] = serializer.data["otp"]
                user_otp.last_used = timezone.now()
                user_otp.save()
                return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)


class CustomUserKeyViewSet(UserKeyViewSet):
    @action(detail=False, methods=["get", "post"], name="Add new FIDO2 Key")
    def fido2(self, request):
        def generate_fido2_registration(user):
            """Returns registration_data, state"""
            server = get_server()
            return server.register_begin(
                {
                    "id": str(user.pk).encode(),
                    "name": user.get_username(),
                    "displayName": user.get_username(),
                },
                get_user_credentials(user),
            )

        if request.method == "GET":
            registration_data, state = generate_fido2_registration(request.user)
            set_expirable_var(request.session, "fido_state", state)
            return Response(registration_data)

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(key_type=UserKey.KeyType.FIDO2, user=self.request.user)
        return Response(serializer.data)


class VerifyFIDO2View(FIDO2Authenticate):
    throttle_classes = (Burst10MinUser,)
    authentication_classes = (TokenAuthentication,)
    permission_classes = (
        permissions.IsAuthenticated,
        HasVerifiedEmail,
    )

    def get_user(self, request):
        return request.user

    def success(self, request, user, key):
        super().success(request, user, key)
        OTPVerifiedToken.objects.get_or_create(otp=key, auth_token=request.auth)
