from django.apps import AppConfig
from django.conf import settings
from django.db.models.signals import post_save


class ConfirmEmailConfig(AppConfig):
    name = "apps.confirm_email"

    def ready(self):
        from . import signals
        from .models import EmailConfirmationCode

        post_save.connect(
            signals.create_confirmation_code_handler,
            sender=settings.AUTH_USER_MODEL,
            dispatch_uid="create_conf_code",
        )
        post_save.connect(
            signals.send_confirmation_code_email_handler,
            sender=EmailConfirmationCode,
            dispatch_uid="send_conf_code_email",
        )
