from django.template.loader import render_to_string
from django.template import Context
from django.core.mail import send_mail

from .conf import LINK_TEMPLATE_INSTANCE, FROM_EMAIL, SUBJECT
from .models import EmailConfirmationCode


def create_confirmation_code_handler(sender, instance, created, raw, **kwargs):
    """
    When a user is first created, create an email confirmation code.
    """
    if created:
        EmailConfirmationCode.objects.create(user=instance)


def send_confirmation_code_email_handler(sender, instance, created, raw, **kwargs):
    """
    Whenever a EmailConfirmationCode is created, we want to send an email
    to the user with the code.
    """
    if created:
        context = {
            "link": LINK_TEMPLATE_INSTANCE.render(
                Context(
                    {
                        "long_code": instance.long_code,
                    }
                )
            ),
            "short_code": instance.short_code,
            "long_code": instance.long_code,
        }
        send_mail(
            subject=SUBJECT,
            recipient_list=[instance.user.email],
            message=render_to_string("confirm_email/email.txt", context),
            from_email=FROM_EMAIL,
            html_message=render_to_string("confirm_email/email.html", context),
        )
