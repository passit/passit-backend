from django.test import TestCase
from django.contrib.auth import get_user_model
from django.utils import timezone
from django.core import mail
from rest_framework.test import APITestCase
from django.urls import reverse
from rest_framework import status

from .models import EmailConfirmationCode


class TestEmailConfirmationCode(APITestCase):
    def setUp(self):
        self.user = get_user_model().objects.create_user("test@test.com", "password")

    def create_code(self):
        return EmailConfirmationCode.objects.create(user=self.user)

    def test_codes_differenet(self):
        first = self.create_code()
        second = self.create_code()
        self.assertNotEqual(first.short_code, second.short_code)
        self.assertNotEqual(first.long_code, second.long_code)

    def test_confirmation_created(self):
        inst = EmailConfirmationCode.objects.get()

        self.assertEqual(inst.user, self.user)
        self.assertGreater(inst.expiration, timezone.now())
        self.assertFalse(self.user.email_confirmed)

    def test_email_send(self):
        inst = EmailConfirmationCode.objects.get()

        self.assertEqual(len(mail.outbox), 1)

        message = mail.outbox[0]
        self.assertIn("Confirm", message.subject)

        self.assertIn(self.user.email, message.to)
        self.assertIn(inst.long_code, message.body)

    def test_confirm_email_long_code(self):
        inst = EmailConfirmationCode.objects.get()

        url = reverse("confirm-email-long-code")
        data = {"id": inst.id, "long_code": inst.long_code}
        response = self.client.put(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        user = get_user_model().objects.get()
        self.assertTrue(user.email_confirmed)

    def test_confirm_email_brute_force(self):
        """Ensure a user can't brute force guessing a short code"""
        inst = EmailConfirmationCode.objects.get()
        url = reverse("confirm-email-short-code")
        self.client.force_login(inst.user)
        data = {"id": inst.id, "short_code": "fake"}
        for _ in range(EmailConfirmationCode.MAX_ATTEMPTS):
            response = self.client.put(url, data, format="json")
            self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        data = {"id": inst.id, "short_code": inst.short_code}
        response = self.client.put(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_406_NOT_ACCEPTABLE)
        self.assertFalse(inst.user.email_confirmed)

    def test_confirm_email_short_request_login(self):
        url = reverse("confirm-email-short-code")
        data = {}
        response = self.client.put(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_confirm_email_short(self):
        """User can log in and use short code"""
        inst = EmailConfirmationCode.objects.get()
        url = reverse("confirm-email-short-code")
        self.client.force_login(inst.user)
        data = {"id": inst.id, "short_code": inst.short_code}
        user = get_user_model().objects.get()
        self.assertFalse(user.email_confirmed)
        response = self.client.put(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        user.refresh_from_db()
        self.assertTrue(user.email_confirmed)

    def test_confirm_email_short_with_o(self):
        """User can confirm short code with case insensative
        and replace O with 0"""
        inst = EmailConfirmationCode.objects.get()
        inst.short_code = "A0AAAA"
        inst.save()
        url = reverse("confirm-email-short-code")
        self.client.force_login(inst.user)
        data = {"id": inst.id, "short_code": "aOAAAA"}
        response = self.client.put(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_expired_confirmation(self):
        inst = EmailConfirmationCode.objects.get()
        inst.expiration = timezone.now()
        inst.save()

        url = reverse("confirm-email-long-code")
        data = {"id": inst.id, "long_code": inst.long_code}
        response = self.client.put(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_406_NOT_ACCEPTABLE)

        user = get_user_model().objects.get()
        self.assertFalse(user.email_confirmed)

    def test_invalid_code(self):
        inst = EmailConfirmationCode.objects.get()

        url = reverse("confirm-email-long-code")
        data = {"id": inst.id, "long_code": "not right"}
        response = self.client.put(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        user = get_user_model().objects.get()
        self.assertFalse(user.email_confirmed)

    def test_request_new_confiramtion(self):
        inst = EmailConfirmationCode.objects.get()
        self.assertEqual(len(mail.outbox), 1)
        url = reverse("request-new-confirmation")
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_login(inst.user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Second code should have been sent now
        self.assertEqual(len(mail.outbox), 2)
