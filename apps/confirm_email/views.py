from django.db.models import F
from django.shortcuts import get_object_or_404

from rest_framework.status import HTTP_406_NOT_ACCEPTABLE, HTTP_400_BAD_REQUEST
from rest_framework.decorators import (
    api_view,
    permission_classes,
    authentication_classes,
)
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from .models import EmailConfirmationCode
from .serializers import (
    EmailConfirmationLongCodeSerializer,
    EmailConfirmationShortCodeSerializer,
)


@api_view(["PUT"])
@authentication_classes([])
@permission_classes([])
def confirm_email_from_long_code(request):
    serializer = EmailConfirmationLongCodeSerializer(data=request.data)
    if not serializer.is_valid():
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)

    inst = get_object_or_404(
        EmailConfirmationCode,
        long_code=serializer.validated_data["long_code"],
    )
    if inst.expired:
        return Response(
            {"message": "Confirmation code has expired."},
            status=HTTP_406_NOT_ACCEPTABLE,
        )

    inst.user.email_confirmed = True
    inst.user.save()
    return Response({"user_id": inst.user.pk})


@api_view(["PUT"])
@permission_classes([IsAuthenticated])
def confirm_email_from_short_code(request):
    """User must be logged in to use this, but wouldn't need a verified email"""
    # Increment attempts for ANY code this user is connected to.
    if request.user and request.user.id:
        EmailConfirmationCode.objects.filter(user=request.user.id).update(
            short_code_attempts=F("short_code_attempts") + 1
        )

    data = request.data
    data["user"] = request.user.id
    serializer = EmailConfirmationShortCodeSerializer(data=data)
    if not serializer.is_valid():
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)

    # Replace O with 0 to make it easier to type in hex codes
    short_code = serializer.validated_data["short_code"].replace("O", "0")
    inst = get_object_or_404(
        EmailConfirmationCode,
        short_code__iexact=short_code,
        user=serializer.validated_data["user"],
    )
    if inst.expired:
        return Response(
            {"message": "Confirmation code has expired."},
            status=HTTP_406_NOT_ACCEPTABLE,
        )

    inst.user.email_confirmed = True
    inst.user.save()
    return Response({"user_id": inst.user.pk})


@api_view(["GET"])
@permission_classes([IsAuthenticated])
def request_new_confirmation(request):
    """Send a new email confirmation email to logged in user"""
    EmailConfirmationCode.objects.create(user=request.user)
    return Response({"message": "Success"})
