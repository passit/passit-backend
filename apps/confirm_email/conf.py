from django.conf import settings
from django.template import Template

DAYS_TILL_EXPIRE = 7

FROM_EMAIL = settings.DEFAULT_FROM_EMAIL

# LINK_TEMPLATE is a Django template that is rendered with the `user`
# and the `code` and should be the link the user can click from
# their email to verify their email.
LINK_TEMPLATE = (
    settings.EMAIL_CONFIRMATION_HOST + "/account/confirm-email/{{ long_code }}"
)
LINK_TEMPLATE_INSTANCE = Template(LINK_TEMPLATE)

SUBJECT = "Passit: Confirm Email"
