from rest_framework import serializers

from .models import EmailConfirmationCode


class EmailConfirmationLongCodeSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmailConfirmationCode
        fields = ["long_code"]
        extra_kwargs = {
            "long_code": {"required": True},
        }


class EmailConfirmationShortCodeSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmailConfirmationCode
        fields = ["short_code", "user"]
        extra_kwargs = {
            "short_code": {"required": True},
        }
