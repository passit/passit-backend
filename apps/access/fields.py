from django.db import models
from django.core.validators import RegexValidator, MinLengthValidator


BASE64_REGEX = r"^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$"


class CiphertextField(models.TextField):
    """Attempts to validate the data is actually ciphertext with a few
    sanity checks."""

    def __init__(self, *args, **kwargs):
        kwargs["validators"] = [
            MinLengthValidator(96),
            RegexValidator(regex=BASE64_REGEX, message="Note base64"),
        ]
        super().__init__(*args, **kwargs)


class PublicKeyField(models.TextField):
    """Public Key field includes some validation to ensure the data is actually
    a public key saved in the correct format
    """

    def __init__(self, *args, **kwargs):
        kwargs["max_length"] = 44
        kwargs["validators"] = [
            RegexValidator(
                regex=BASE64_REGEX,
                message="Not base64",
            ),
            MinLengthValidator(44),
        ]
        super().__init__(*args, **kwargs)
