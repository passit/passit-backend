from django.urls import reverse
from django.core import mail
from rest_framework.test import APITestCase
from passit.test_client import PassitTestMixin
from model_bakery import baker

from ..models import User, BackupCodeEmailToken
from simple_asym import Asym
from simple_asym.crypto import export_private_key, authenticated_encryption

import base64
import json


class TestAuthAPI(PassitTestMixin, APITestCase):
    def test_reset_password_failure(self):
        url = reverse("reset-password")
        user = baker.make(User)
        data = {"email": user.email}
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, 200)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(BackupCodeEmailToken.objects.count(), 0)

    def test_reset_password(self):
        url = reverse("reset-password")
        user = baker.make(User, private_key_backup="a")
        data = {"email": user.email}
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, 200)
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(BackupCodeEmailToken.objects.count(), 1)

    def test_reset_password_handles_urlencode(self):
        """ " An email like fun+cool@example.com should work"""
        url = reverse("reset-password")
        email = "fun+cool@example.com"
        baker.make(User, email=email, private_key_backup="a")
        data = {"email": email}
        self.client.post(url, data)
        # + is not a urlencoded character
        self.assertNotIn("+", mail.outbox[1].body)

    def test_reset_password_confirm_failure(self):
        url = reverse("reset-password")
        user = baker.make(User, private_key_backup="a")
        data = {"email": user.email}
        res = self.client.post(url, data)

        url = reverse("reset-password-verify")
        data = {
            "email": user.email,
            "code": "abc",
        }
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, 401)

    def test_reset_password_confirm(self):
        url = reverse("reset-password")
        user = baker.make(User, private_key_backup="a")
        data = {"email": user.email}
        res = self.client.post(url, data)

        code = BackupCodeEmailToken.objects.first()

        url = reverse("reset-password-verify")
        data = {
            "email": user.email,
            "code": code.code,
        }
        res = self.client.post(url, data)
        self.assertEqual(res.data["private_key_backup"], "a")

    def test_integration(self):
        asym = Asym()
        user_password = "hunter2"
        user_backup_code = "d03600f3d385b7"
        private_key, public_key = asym.make_rsa_keys(password=user_password)
        private_key_backup = export_private_key(asym.key_pair, user_backup_code)
        user = baker.make(
            User,
            private_key=private_key,
            public_key=public_key,
            private_key_backup=private_key_backup,
        )

        # Request backup
        url = reverse("reset-password")
        data = {"email": user.email}
        self.client.post(url, data)

        # Ensure code sent to email
        code = BackupCodeEmailToken.objects.filter(user=user).first()
        self.assertIn(code.code, mail.outbox[-1].body)

        # Verify reset password code
        url = reverse("reset-password-verify")
        data = {
            "email": user.email,
            "code": code.code,
        }
        res = self.client.post(url, data)

        # Decrypt private key with backup code
        asym.set_key_pair(
            res.data["public_key"], res.data["private_key_backup"], user_backup_code
        )
        # Encrypt something to prove access to private key
        random_string = "a" * 20
        encrypted_message = authenticated_encryption(
            base64.b64decode(res.data["server_public_key"]),
            asym.key_pair["private_key"],
            random_string.encode(),
        )

        # Send message to server to prove private key access
        url = reverse("reset-password-login")
        data = {
            "email": user.email,
            "code": code.code,
            "message": encrypted_message,
        }
        res = self.client.post(url, data)
        self.assertContains(res, "token")
        self.client.force_login(user)

        # Change password using backup method
        url = reverse("change-password-backup")
        data = {
            "secret_through_set": [],
            "group_user_set": [],
            "code": code.code,
            "user": {
                "client_salt": "a" * 24,
                "password": "a",
                "private_key": "a" * 120,
                "private_key_backup": "a" * 120,
                "public_key": "a" * 44,
            },
        }
        res = self.client.post(url, json.dumps(data), content_type="application/json")
        self.assertEqual(res.status_code, 200)
