from constance import config
import fnmatch


def is_acceptable_email(email: str) -> bool:
    """Check if email complies with server policy
    For example email whitelists."""
    whitelist = config.REGISTRATION_WHITELIST

    if not whitelist:
        return True

    for item in whitelist.split(","):
        if fnmatch.fnmatch(email, item):
            return True
    return False
