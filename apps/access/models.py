import secrets
import datetime
from django.db import models
from django.core.mail import send_mail
from django.core.validators import RegexValidator, MinLengthValidator
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.conf import settings
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.utils.html import strip_tags
from simple_asym import Asym

from ..secrets.models import Secret
from .fields import BASE64_REGEX, CiphertextField, PublicKeyField


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, private_key, public_key, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError("The given email must be set")
        email = self.normalize_email(email)

        if private_key is None and public_key is None:
            asym = Asym()
            private_key, public_key = asym.make_rsa_keys(password=password)

        user = self.model(
            email=email, private_key=private_key, public_key=public_key, **extra_fields
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(
        self, email, password, private_key=None, public_key=None, **extra_fields
    ):
        extra_fields.setdefault("is_staff", False)
        return self._create_user(
            email, password, private_key, public_key, **extra_fields
        )

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault("is_staff", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")

        # Must make RSA keys
        asym = Asym()
        private_key, public_key = asym.make_rsa_keys(password=password)
        # The password won't be usable to login with the client due to lack of
        # hashing.

        return self._create_user(
            email, password, private_key, public_key, **extra_fields
        )

    def get_by_natural_key(self, username):
        case_insensitive_username_field = "{}__iexact".format(self.model.USERNAME_FIELD)
        return self.get(**{case_insensitive_username_field: username})


class User(AbstractBaseUser):
    first_name = models.CharField(_("first name"), max_length=30, blank=True)
    last_name = models.CharField(_("last name"), max_length=30, blank=True)
    email = models.EmailField(_("email address"), unique=True)
    is_staff = models.BooleanField(
        _("staff status"),
        default=False,
        help_text=_("Designates whether the user can log into this admin site."),
    )
    is_superuser = models.BooleanField(default=False)
    is_active = models.BooleanField(
        _("active"),
        default=True,
        help_text=_(
            "Designates whether this user should be treated as active. "
            "Unselect this instead of deleting accounts."
        ),
    )
    email_confirmed = models.BooleanField(
        _("email confirmed?"),
        default=False,
        help_text=_("Designates whether this user has confirmed access to their email"),
    )
    date_joined = models.DateTimeField(_("date joined"), default=timezone.now)
    client_salt = models.CharField(
        max_length=24,
        validators=[
            MinLengthValidator(24),
            RegexValidator(BASE64_REGEX),
        ],
    )
    public_key = PublicKeyField()
    private_key = models.TextField(
        max_length=120,
        validators=[
            MinLengthValidator(120),
            RegexValidator(BASE64_REGEX),
        ],
        help_text="Must be encrypted using passphrase",
    )
    private_key_backup = models.TextField(
        max_length=120,
        validators=[
            MinLengthValidator(120),
            RegexValidator(BASE64_REGEX),
        ],
        help_text="Secondary method of decrypting private key",
        blank=True,
        null=True,
    )
    private_key_backup_date = models.DateTimeField(blank=True, null=True)
    opt_in_error_reporting = models.BooleanField(default=settings.ERROR_REPORT_DEFAULT)

    objects = UserManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _("user")
        verbose_name_plural = _("users")

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = "%s %s" % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        "Returns the short name for the user."
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def has_module_perms(self, app_label):
        return True

    def has_perm(self, perm, obj=None):
        return True

    @property
    def mfa_required(self) -> bool:
        """Does user require MFA to log in"""
        return self.userkey_set.filter(properties__is_active=True).exists()

    @property
    def valid_auth(self) -> bool:
        return self.userkey_set.all().values_list("key_type", flat=True).distinct()

    def delete(self):
        """Also delete any personal secrets - but don't touch group secrets"""
        # Get id's first because querysets are lazy
        personal_secrets_ids = list(
            Secret.objects.filter(
                secret_through_set__group=None,
                secret_through_set__user=self,
            ).values_list("id", flat=True)
        )
        super().delete()
        personal_secrets = Secret.objects.filter(id__in=personal_secrets_ids)
        for personal_secret in personal_secrets:
            # Only delete if secret has no attachments
            if not personal_secret.secret_through_set.exists():
                personal_secret.delete()

    def save(self, *args, **kwargs):
        # Update private key backup date only if private key backup changes
        if self.pk is not None:
            orig = User.objects.get(pk=self.pk)
            if orig.private_key_backup != self.private_key_backup:
                self.private_key_backup_date = timezone.now()
        elif self.private_key_backup:
            self.private_key_backup_date = timezone.now()

        self.email = self.email.lower()
        return super(User, self).save(*args, **kwargs)

    def __str__(self):
        return self.email


class ServerKeyPair(models.Model):
    """This is only used to verify authentication tags from the client"""

    private_key = models.CharField(max_length=255)
    public_key = models.CharField(max_length=255)

    @classmethod
    def create(cls):
        asym = Asym()
        private_key, public_key = asym.make_rsa_keys()
        return cls(private_key=private_key, public_key=public_key)


class BackupCodeEmailToken(models.Model):
    """
    If a user forgets their password (but has backup code) they can request
    an email with a token from this model. This token will get them limited
    access to download their private key that is encrypted via the backup code.
    If the user can then prove they have the original private key (by decrypting)
    they can then be given full access. This token acts like a one time password
    token but it does not give full access to anything.
    """

    code = models.CharField(
        max_length=80,
        blank=False,
        null=False,
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=False, blank=False
    )
    expiration = models.DateTimeField(
        blank=False,
        null=False,
    )

    @classmethod
    def create(cls, user):
        code = secrets.token_urlsafe(40)
        expire = timezone.now() + datetime.timedelta(hours=1)
        return cls(code=code, user=user, expiration=expire)

    @classmethod
    def check_code(cls, email, code):
        """Check if a code is valid or not"""
        result = cls.objects.filter(
            user__email=email,
            code=code,
            expiration__gte=timezone.now(),
        ).first()
        if result:
            return result
        return False


class GroupManager(models.Manager):
    def create(
        self,
        user,
        name,
        slug,
        public_key,
        key_ciphertext,
        private_key_ciphertext,
        **extra_fields,
    ):
        """Set up a group with a access group user (required)"""
        group = self.model(name=name, slug=slug, public_key=public_key, **extra_fields)
        group.save()

        GroupUser.objects.create(
            user=user,
            group=group,
            is_group_admin=True,
            key_ciphertext=key_ciphertext,
            private_key_ciphertext=private_key_ciphertext,
        )
        return group


class Group(models.Model):
    name = models.CharField(max_length=1000)
    slug = models.SlugField(max_length=1000, default="default-slug", blank=True)
    organization = models.ForeignKey(
        "organizations.Organization", blank=True, null=True, on_delete=models.CASCADE
    )
    users = models.ManyToManyField(
        User, through="access.GroupUser", blank=True, related_name="groups"
    )
    public_key = PublicKeyField()

    objects = GroupManager()

    class Meta:
        verbose_name = "Group"
        unique_together = ("name", "organization")

    def __str__(self):
        return self.name

    def get_keys(self, user):
        group_user = self.groupuser.get(user=user)
        aes = group_user.key_ciphertext
        private = group_user.encrypted_private_key
        return aes, private


class GroupUser(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    is_group_admin = models.BooleanField(
        default=True, help_text="Is able to add/remove members"
    )
    key_ciphertext = CiphertextField()
    private_key_ciphertext = CiphertextField()

    class Meta:
        unique_together = ("user", "group")

    def __str__(self):
        return "{} - {}".format(self.user, self.group)


class GroupUserInvite(models.Model):
    """Group User pending invite.
    We need to create the actual GroupUser object and share actual secrets even
    before the invitee accepts."""

    inviter = models.ForeignKey(User, on_delete=models.CASCADE)
    invitee_groupuser = models.ForeignKey(GroupUser, on_delete=models.CASCADE)

    def __str__(self):
        return "{} invited {} to {}".format(
            self.inviter,
            self.invitee_groupuser.user,
            self.invitee_groupuser.group,
        )

    def send_invite_email(self):
        user = strip_tags(self.invitee_groupuser.user.email)
        group = strip_tags(self.invitee_groupuser.group.name)
        message = f"""Hi {user},

{self.inviter} created the group "{group}" on Passit and would like you to join. Once you join {group}, you’ll be able to start sharing passwords with {self.invitee_groupuser.user} and others.

Click to join! {settings.HOSTNAME}"""
        # TODO create html message version
        send_mail(
            "Passit group invite",
            message,
            settings.DEFAULT_FROM_EMAIL,
            [user],
            html_message=message,
        )
