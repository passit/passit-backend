# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-05-07 16:13
from __future__ import unicode_literals

import apps.access.models
from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):
    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="User",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("password", models.CharField(max_length=128, verbose_name="password")),
                (
                    "last_login",
                    models.DateTimeField(
                        blank=True, null=True, verbose_name="last login"
                    ),
                ),
                (
                    "first_name",
                    models.CharField(
                        blank=True, max_length=30, verbose_name="first name"
                    ),
                ),
                (
                    "last_name",
                    models.CharField(
                        blank=True, max_length=30, verbose_name="last name"
                    ),
                ),
                (
                    "email",
                    models.EmailField(
                        max_length=254, unique=True, verbose_name="email address"
                    ),
                ),
                (
                    "is_staff",
                    models.BooleanField(
                        default=False,
                        help_text="Designates whether the user can log into this admin site.",
                        verbose_name="staff status",
                    ),
                ),
                (
                    "is_active",
                    models.BooleanField(
                        default=True,
                        help_text="Designates whether this user should be treated as active. Unselect this instead of deleting accounts.",
                        verbose_name="active",
                    ),
                ),
                (
                    "date_joined",
                    models.DateTimeField(
                        default=django.utils.timezone.now, verbose_name="date joined"
                    ),
                ),
                ("client_alg", models.CharField(default="SHA256", max_length=20)),
                (
                    "client_iterations",
                    models.PositiveIntegerField(
                        default=24000,
                        validators=[django.core.validators.MinValueValidator(10000)],
                    ),
                ),
                (
                    "client_salt",
                    models.CharField(
                        max_length=512,
                        validators=[django.core.validators.MinLengthValidator(12)],
                    ),
                ),
                (
                    "public_key",
                    apps.access.models.PublicKeyField(
                        max_length=900,
                        validators=[
                            django.core.validators.RegexValidator(
                                message="Not a public key",
                                regex="^-----BEGIN (RSA )?PUBLIC KEY-----.*",
                            ),
                            django.core.validators.RegexValidator(
                                message="Not a public key",
                                regex=".*-----END (RSA )?PUBLIC KEY-----$",
                            ),
                            django.core.validators.MinLengthValidator(400),
                        ],
                    ),
                ),
                (
                    "private_key",
                    models.TextField(
                        help_text="Must be encrypted using passphrase",
                        max_length=8000,
                        validators=[
                            django.core.validators.RegexValidator(
                                message="Not a private key",
                                regex="^-----BEGIN ENCRYPTED PRIVATE KEY-----.*",
                            ),
                            django.core.validators.RegexValidator(
                                message="Not a private key",
                                regex=".*-----END ENCRYPTED PRIVATE KEY-----$",
                            ),
                            django.core.validators.MinLengthValidator(1500),
                        ],
                    ),
                ),
            ],
            options={
                "verbose_name_plural": "users",
                "verbose_name": "user",
            },
            managers=[
                ("objects", apps.access.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name="Group",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.CharField(max_length=1000)),
                ("slug", models.SlugField(default="default-slug", max_length=1000)),
                (
                    "public_key",
                    apps.access.models.PublicKeyField(
                        max_length=900,
                        validators=[
                            django.core.validators.RegexValidator(
                                message="Not a public key",
                                regex="^-----BEGIN (RSA )?PUBLIC KEY-----.*",
                            ),
                            django.core.validators.RegexValidator(
                                message="Not a public key",
                                regex=".*-----END (RSA )?PUBLIC KEY-----$",
                            ),
                            django.core.validators.MinLengthValidator(400),
                        ],
                    ),
                ),
            ],
            options={
                "verbose_name": "Group",
            },
        ),
        migrations.CreateModel(
            name="GroupUser",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "is_group_admin",
                    models.BooleanField(
                        default=False, help_text="Is able to add/remove members"
                    ),
                ),
                (
                    "key_ciphertext",
                    apps.access.models.CiphertextField(
                        validators=[django.core.validators.MinLengthValidator(400)]
                    ),
                ),
                (
                    "private_key_ciphertext",
                    apps.access.models.CiphertextField(
                        validators=[django.core.validators.MinLengthValidator(400)]
                    ),
                ),
                (
                    "group",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to="access.Group"
                    ),
                ),
                (
                    "user",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="Organization",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.CharField(max_length=1000)),
            ],
        ),
        migrations.AddField(
            model_name="group",
            name="organization",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                to="access.Organization",
            ),
        ),
        migrations.AddField(
            model_name="group",
            name="users",
            field=models.ManyToManyField(
                blank=True,
                related_name="groups",
                through="access.GroupUser",
                to=settings.AUTH_USER_MODEL,
            ),
        ),
        migrations.AddField(
            model_name="user",
            name="organizations",
            field=models.ManyToManyField(blank=True, to="access.Organization"),
        ),
        migrations.AlterUniqueTogether(
            name="groupuser",
            unique_together=set([("user", "group")]),
        ),
        migrations.AlterUniqueTogether(
            name="group",
            unique_together=set([("name", "organization")]),
        ),
    ]
