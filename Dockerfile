FROM python:3.12 AS build-python
ENV PYTHONUNBUFFERED=1 \
  PORT=8080 \
  POETRY_VIRTUALENVS_CREATE=false \
  PIP_DISABLE_PIP_VERSION_CHECK=on
ARG IGNORE_DEV_DEPS

RUN mkdir /code
WORKDIR /code

RUN apt-get update && apt-get install -y libsasl2-dev libldap2-dev 
RUN pip install poetry
COPY poetry.lock pyproject.toml /code/
RUN poetry install --no-interaction --no-ansi $(test "$IGNORE_DEV_DEPS" = "True" && echo "--no-dev")

FROM python:3.12-slim
ENV PYTHONUNBUFFERED=1 \
  PORT=8080

RUN apt-get update && apt-get install -y libsodium23 libldap2-dev libxml2 && apt-get clean && rm -rf /var/lib/apt/lists/*

WORKDIR /code

COPY --from=build-python /usr/local/lib/python3.12/site-packages/ /usr/local/lib/python3.12/site-packages/
COPY --from=build-python /usr/local/bin/ /usr/local/bin/

EXPOSE 8080

COPY . /code/

CMD ["./bin/start.sh"]
