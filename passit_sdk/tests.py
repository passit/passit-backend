import unittest
from .hashing import hash_password


class TestHash(unittest.TestCase):
    def test_password_hash(self):
        password = "123456"
        salt = "1sFaXL6QKQg3okh2PPD9+Q=="
        (hashed, salt) = hash_password(password, salt)
        self.assertEqual(hashed, "jeSSh+aHIwtHH5uVBcVXLPOhleZXtRSWu31Ihjanlw8=")

    def test_long_password_hash(self):
        password = "Â1234567890123456789" * 3
        salt = "1sFaXL6QKQg3okh2PPD9+Q=="
        (hashed, salt) = hash_password(password, salt)
        self.assertEqual(hashed, "+PoSdFe40KZ26bG4hPU3IPdfx8FWjVP0CeVRl3+VviQ=")

    def test_make_salt(self):
        password = "1"
        (hashed, salt) = hash_password(password)

        # Hash it again with the same salt. It should produce the same results
        (hashed2, salt2) = hash_password(password, salt)
        self.assertEqual(hashed, hashed2)
        self.assertEqual(salt, salt)
