class PassitSDKException(Exception):
    pass


class PassitAuthenticationRequired(PassitSDKException):
    pass


class PassitAPIException(PassitSDKException):
    """These exceptions are related to API interaction with the backend
    server. They must contain the full response object in `res`

    Use like:

    ```
        if res.status_code != 200:
            raise PassitAPIException(res, 'Unable to check username')
    ```
    """

    def __init__(self, res, message=None):
        self.res = res
        self.message = message

    def __str__(self):
        return repr(self.message)
