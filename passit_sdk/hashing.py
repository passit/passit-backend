from typing import Tuple
import base64
from simple_asym.crypto import hash_password as simple_asym_hash, make_salt


def generate_salt() -> str:
    """Returns salt"""
    return base64.b64encode(make_salt()).decode()


def hash_password(
    password: str,
    salt: str = None,
) -> Tuple[str, str]:
    """
    Hash a password. Set salt or leave None to generate one.

    :return: (password hash, salt)
    """
    if not salt:
        salt = generate_salt()
    hash = base64.b64encode(simple_asym_hash(password, salt)).decode()
    return (hash, salt)
